#include <iostream>
#include <algorithm>
#include <fstream>
#include <iomanip>
#include <chrono>

#include <opencv2/core/core.hpp>

#include <srrg_messages/message_reader.h>
#include <srrg_messages/pinhole_image_message.h>
#include <srrg_messages/sensor_message_sorter.h>
#include <srrg_messages/message_timestamp_synchronizer.h>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

using namespace std;
using namespace srrg_core;

const char* banner [] = {
  "stereo_orbslam w/ txtio input",
  "",
  "usage: stereo_app_txtio -v <vocabulary> -s <settings> -tl <topic-left> -tr <topic-right> -data <path/to/dataset> -o <traj-kitti-format>",
  "-tl       <string>        left image topic name",
  "-tr       <string>        right image topic name",
  "-data     <string>        path to dataset.txtio",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-ng       <flag>          no gui",
  "-o        <string>        otuput trajectory in KITTI format",
  "-h        <flag>          this help",
  0
};


int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }
  int c = 1;

  //params
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  std::string dataset_filename = "";
  std::string left_topic = "";
  std::string right_topic = "";
  std::string output_filename = "orb_slam_output-kitti.txt";
  std::string timings_filename = "timings.txt";
  bool use_gui = true;

  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-tl")) {
      c++;
      left_topic = argv[c];
    } else if (!strcmp(argv[c], "-tr")) {
      c++;
      right_topic = argv[c];
    } else if (!strcmp(argv[c], "-data")) {
      c++;
      dataset_filename = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      timings_filename = argv[c];
    }
    c++;
  }

  std::cerr<<"left_topic           (-tl):         "<< left_topic << std::endl;
  std::cerr<<"right_topic          (-tr):         "<< right_topic << std::endl;
  std::cerr<<"dataset_filename     (-data):       "<< dataset_filename << std::endl;
  std::cerr<<"vocabulary           (-v):          "<< vocabulary_filename << std::endl;
  std::cerr<<"settings             (-s):          "<< settings_filename << std::endl;
  std::cerr<<"output               (-o):          "<< output_filename << std::endl;
  std::cerr<<"use_gui              (-ng):         "<< use_gui << std::endl;
  std::cerr<<"timings_filename     (-t):          "<< timings_filename << std::endl;

  if(vocabulary_filename.empty() ||
     left_topic.empty() ||
     right_topic.empty() ||
     settings_filename.empty()   ||
     dataset_filename.empty()) {
    std::cerr << "Some Parameters are missing. See above." << std::endl;
    return 1;
  }

  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::STEREO,use_gui);

  //bdc, txtio reader
  MessageReader message_reader;
  message_reader.open(dataset_filename);

  MessageTimestampSynchronizer synchronizer;
  std::vector<std::string> camera_topics_synchronized(0);
  camera_topics_synchronized.push_back(left_topic);
  camera_topics_synchronized.push_back(right_topic);
  synchronizer.setTimeInterval(0.001);
  synchronizer.setTopics(camera_topics_synchronized);

  //ds start playback
  srrg_core::BaseMessage* message = 0;
  size_t processed_frames = 0;
  double mean_frame_time = 0.f;
  double total_processing_time = srrg_core::getTime();
  while ((message = message_reader.readMessage())) {
    srrg_core::BaseSensorMessage* sensor_message = dynamic_cast<srrg_core::BaseSensorMessage*>(message);
    assert(sensor_message);
    sensor_message->untaint();

    //bdc add to synchronizer
    if (sensor_message->topic() == left_topic) {
      synchronizer.putMessage(sensor_message);
    } else if (sensor_message->topic() == right_topic) {
      synchronizer.putMessage(sensor_message);
    } else {
      delete sensor_message;
    }

    //bdc if we have a synchronized package of sensor messages ready
    if (synchronizer.messagesReady()) {

      //bdc buffer sensor data
      srrg_core::PinholeImageMessage* image_message_left  = dynamic_cast<srrg_core::PinholeImageMessage*>(synchronizer.messages()[0].get());
      srrg_core::PinholeImageMessage* image_message_right = dynamic_cast<srrg_core::PinholeImageMessage*>(synchronizer.messages()[1].get());

      //bdc buffer images
      cv::Mat intensity_image_left_rectified;
      if(image_message_left->image().type() == CV_8UC3){
        cvtColor(image_message_left->image(), intensity_image_left_rectified, CV_BGR2GRAY);
      } else {
        intensity_image_left_rectified = image_message_left->image();
      }
      cv::Mat intensity_image_right_rectified;
      if (image_message_right->image().type() == CV_8UC3) {
        cvtColor(image_message_right->image(), intensity_image_right_rectified, CV_BGR2GRAY);
      } else {
        intensity_image_right_rectified = image_message_right->image();
      }

      Eigen::Isometry3f gt_pose = Eigen::Isometry3f::Identity();
      if(image_message_right->hasOdom()) {
        gt_pose = image_message_right->odometry();
      }

      //bdc Pass the images to the SLAM system
      double t0 = srrg_core::getTime();
      SLAM.TrackStereo(intensity_image_left_rectified,
                       intensity_image_right_rectified,
                       image_message_left->timestamp(),
                       gt_pose);
      mean_frame_time += (srrg_core::getTime() - t0);
      ++processed_frames;

      image_message_left->release();
      image_message_right->release();
      synchronizer.reset();

      //bdc free image references
      intensity_image_left_rectified.release();
      intensity_image_right_rectified.release();
    }
  }


  message_reader.close();

  // Stop all threads
  SLAM.Shutdown();
  total_processing_time = (srrg_core::getTime() - total_processing_time);

  mean_frame_time = mean_frame_time / (double)(processed_frames);
  std::cout << "frame processed [ " << processed_frames << " ]\n";
  std::cout << "total time [ " << total_processing_time << " ]\n";
  std::cout << "mean frame-time [ " << mean_frame_time << " ] -- mean fps [ " << 1./mean_frame_time << " ]\n";

  {
    std::cout << "dumping timings in [ " << timings_filename << " ]\n";
    std::ofstream stream(timings_filename, std::ofstream::out);
    stream << "total_frames= " << processed_frames << " total_compute_time= " << total_processing_time
           << " mean_frame_time= " << mean_frame_time << " mean_frame_hz= " << 1./mean_frame_time
           << std::endl;
    stream.close();
  }

  // Save camera trajectory
  SLAM.SaveTrajectoryKITTI(output_filename);
  std::cerr << "Saved output Trajetory in " << output_filename << std::endl;

  return 0;
}
