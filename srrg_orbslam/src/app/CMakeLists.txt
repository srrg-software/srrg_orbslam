# Monocular APP
add_executable(monocular_app
  monocular_app.cpp
  )
target_link_libraries(monocular_app
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )


# RGB-D APP
add_executable(rgbd_app
  rgbd_app.cpp
  )
target_link_libraries(rgbd_app
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )

# Stereo APP
add_executable(stereo_app
  stereo_app.cpp
  )
target_link_libraries(stereo_app
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )


# Monocular APP TXTIO
add_executable(monocular_app_txtio
  monocular_app_txtio.cpp
  )
target_link_libraries(monocular_app_txtio
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )

# RGB-D APP TXTIO
add_executable(rgbd_app_txtio
  rgbd_app_txtio.cpp
  )
target_link_libraries(rgbd_app_txtio
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )

# Stereo APP TXTIO
add_executable(stereo_app_txtio
  stereo_app_txtio.cpp
  )
target_link_libraries(stereo_app_txtio
  srrg_orbslam_system_library
  srrg_orbslam_viewer_library
  ${catkin_LIBRARIES}
  )
