#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <opencv2/core/core.hpp>

#include <srrg_messages/message_reader.h>
#include <srrg_messages/pinhole_image_message.h>
#include "srrg_messages/sensor_message_sorter.h"
#include "srrg_messages/message_timestamp_synchronizer.h"
#include <srrg_image_utils/depth_utils.h>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

using namespace std;
using namespace srrg_core;

const char* banner [] = {
  "rgbd_orbslam w/ txtio input",
  "",
  "usage: rgbd_app_txtio -rgbt <rgb-topic> -t <depth-topic> -v <vocabulary> -s <settings> -data <path/to/dataset.txtio>",
  "-t        <string>        depth topic name",  
  "-rgbt     <string>        rgb topic name",
  "-data     <string>        path to dataset.txtio",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-ng       <flag>          no gui",
  "-o        <string>        output trajectory TUM format",  
  "-h        <flag>          this help",
  0
};

int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }
  int c = 1;

  //params
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  std::string dataset_filename = "";
  std::string rgb_topic_name = "";
  std::string depth_topic_name = "";
  std::string output_filename = "ORBSLAM2_rgbd_output.txt";
  bool use_gui = true;

  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-data")) {
      c++;
      dataset_filename = argv[c];
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 

  std::cerr<<"depth_topic_name     (-t):          "<< depth_topic_name << std::endl;
  std::cerr<<"rgb_topic_name       (-rgbt):       "<< rgb_topic_name << std::endl;
  std::cerr<<"dataset_filename     (-data):       "<< dataset_filename << std::endl;
  std::cerr<<"vocabulary           (-v):          "<< vocabulary_filename << std::endl;
  std::cerr<<"settings             (-s):          "<< settings_filename << std::endl;
  std::cerr<<"output_filename      (-o):          "<< output_filename << std::endl;
  std::cerr<<"use_gui              (-ng):         "<< use_gui << std::endl;  
  
  if(vocabulary_filename.empty() ||
     settings_filename.empty()   ||
     dataset_filename.empty()) {
    return 1;
  }

  
  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::RGBD,use_gui);

  //bdc, txtio reader
  MessageReader reader;
  reader.open(dataset_filename);

  MessageTimestampSynchronizer synchronizer;

  std::vector<std::string> depth_plus_rgb_topic;
  depth_plus_rgb_topic.push_back(depth_topic_name);
  depth_plus_rgb_topic.push_back(rgb_topic_name);
  
  synchronizer.setTopics(depth_plus_rgb_topic);
  synchronizer.setTimeInterval(0.03);

  size_t processed_frames = 0;
  double mean_frame_time = 0.f;
  double total_processing_time = srrg_core::getTime();

  while (reader.good()){
    BaseMessage* msg = reader.readMessage();
    if (!msg) {
      continue;
    }
    
    BaseImageMessage* base_img = dynamic_cast<BaseImageMessage*>(msg);
    if (!base_img) {
      delete msg;
      continue;
    }
    
    synchronizer.putMessage(base_img);
    
    if (!synchronizer.messagesReady())
      continue;
    
    PinholeImageMessage *depth_msg = 0, *rgb_msg = 0;
    
    depth_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[0].get());
    if (!depth_msg)
      throw std::runtime_error("depth msg expected. Shall thou burn in hell.");

    rgb_msg = dynamic_cast<PinholeImageMessage*>(synchronizer.messages()[1].get());

    if (!depth_msg || !rgb_msg) {
      continue;
    }
    
    cv::Mat rgb_image, depth_image;
    depth_msg->image().copyTo(depth_image);
    rgb_msg->image().copyTo(rgb_image);

    //bdc, black magic to go back to original image
    cv::Mat converted_depth_TUM = depth_image*5;    

    Eigen::Isometry3f gt_pose = Eigen::Isometry3f::Identity(); 
    if(depth_msg->hasOdom()) {
      gt_pose = depth_msg->odometry();
    }
        
    // Pass the image to the SLAM system
    double t0 = srrg_core::getTime();
    SLAM.TrackRGBD(rgb_image,converted_depth_TUM,depth_msg->timestamp(), gt_pose);
    mean_frame_time += (srrg_core::getTime() - t0);
    ++processed_frames;
  }
  total_processing_time = (srrg_core::getTime() - total_processing_time);

  // Stop all threads
  SLAM.Shutdown();

  mean_frame_time = mean_frame_time / (double)(processed_frames);
  std::cout << "frame processed [ " << processed_frames << " ]\n";
  std::cout << "total time [ " << total_processing_time << " ]\n";
  std::cout << "mean frame-time [ " << mean_frame_time << " ] -- mean fps [ " << 1./mean_frame_time << " ]\n";

  // Save camera trajectory
  SLAM.SaveTrajectoryTUM(output_filename);
  const std::string key_frame_output_filename = "key_frame_" + output_filename;
  SLAM.SaveKeyFrameTrajectoryTUM(key_frame_output_filename);   

  return 0;
}

