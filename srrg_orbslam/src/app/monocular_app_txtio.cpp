#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <opencv2/core/core.hpp>
#include <srrg_messages/message_reader.h>
#include <srrg_messages/pinhole_image_message.h>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

using namespace std;
using namespace srrg_core;

const char* banner [] = {
  "monocular_orbslam w/ txtio input",
  "",
  "usage: monocular_app_txtio -rgbt <rgb-topic> -v <vocabulary> -s <settings> -data <path/to/dataset.txtio>",
  "-rgbt     <string>        rgb topic name",
  "-data     <string>        path to dataset.txtio",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-ng       <flag>          no gui",
  "-o        <string>        output trajectory TUM format",  
  "-h        <flag>          this help",
  0
};

int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
    return 1;
  }
  int c = 1;

  //params
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  std::string dataset_filename = "";
  std::string rgb_topic_name = "";
  std::string output_filename = "ORBSLAM2_monocular_output.txt";
  bool use_gui = true;

  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-data")) {
      c++;
      dataset_filename = argv[c];
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-o")) {
      c++;
      output_filename = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 

  std::cerr<<"rgb_topic_name   (-rgbt):       "<< rgb_topic_name << std::endl;
  std::cerr<<"dataset_filename (-data):       "<< dataset_filename << std::endl;
  std::cerr<<"vocabulary       (-v):          "<< vocabulary_filename << std::endl;
  std::cerr<<"settings         (-s):          "<< settings_filename << std::endl;
  std::cerr<<"use_gui          (-ng):         "<< use_gui << std::endl;  
  std::cerr<<"output_filename  (-o):          "<< output_filename << std::endl;  
  
  if(vocabulary_filename.empty() ||
     rgb_topic_name.empty() ||
     settings_filename.empty() ||
     dataset_filename.empty()) {
    return 1;
  }
  
  // Create SLAM system. It initializes all system threads and gets ready to process frames.
  ORB_SLAM2::System SLAM(vocabulary_filename,settings_filename,ORB_SLAM2::System::MONOCULAR,use_gui);
    
  //bdc, txtio reader
  MessageReader reader;
  reader.open(dataset_filename);
  
  //bdc, sequential txtio reading
  while (reader.good()) {
    BaseMessage* msg = reader.readMessage();
    if (!msg)
      continue;
    
    PinholeImageMessage  *rgb_msg = 0;

    rgb_msg = dynamic_cast<PinholeImageMessage*>(msg);
    if(!rgb_msg)
      continue;
    if(rgb_msg->topic() != rgb_topic_name)
      continue;

    cv::Mat rgb_image;
    rgb_msg->image().copyTo(rgb_image);

    Eigen::Isometry3f gt_pose = Eigen::Isometry3f::Identity(); 
    if(rgb_msg->hasOdom()) {
      gt_pose = rgb_msg->odometry();
    }
        
    // Pass the image to the SLAM system
    SLAM.TrackMonocular(rgb_image,rgb_msg->timestamp(), gt_pose);

  }
  
  // Stop all threads
  SLAM.Shutdown();

  // Save camera trajectory
  SLAM.SaveKeyFrameTrajectoryTUM(output_filename);

  return 0;
}
