add_library(srrg_orbslam_optimizer_library SHARED
  Optimizer.cc
)

target_link_libraries(srrg_orbslam_optimizer_library
  ${OpenCV_LIBS}
)
