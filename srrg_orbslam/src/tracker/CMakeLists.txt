add_library(srrg_orbslam_tracker_library SHARED
  Tracking.cc
)

target_link_libraries(srrg_orbslam_tracker_library
  srrg_orbslam_solver_library
  srrg_orbslam_optimizer_library
  )
