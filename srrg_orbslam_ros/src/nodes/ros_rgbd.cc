#include <iostream>
#include <algorithm>
#include <fstream>
#include <chrono>

#include <ros/ros.h>
#include <cv_bridge/cv_bridge.h>
#include <message_filters/subscriber.h>
#include <message_filters/time_synchronizer.h>
#include <message_filters/sync_policies/approximate_time.h>

#include <opencv2/core/core.hpp>

#include <srrg_system_utils/system_utils.h>
#include <system/System.h>

#include "tf/transform_broadcaster.h"

using namespace std;

std::string tf_frame, tf_child_frame;
Eigen::Matrix4f Camera_T(Eigen::Matrix4f::Identity());

class ImageGrabber {
public:
  ImageGrabber(ORB_SLAM2::System* pSLAM):mpSLAM(pSLAM){}
  void GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD);
  ORB_SLAM2::System* mpSLAM;
};

const char* banner [] = {
  "rgbd_orbslam",
  "",
  "usage: rgbd_node -v <vocabulary> -s <settings> -t <depth-topic> -rgbt <rgb-topic>",
  "-t        <string>        topic name of depth image, default: '/camera/depth/image_raw'",
  "-rgbt     <string>        topic name of rgb image, default: '/camera/rgb/image_raw'",
  "-v        <string>        path to vocabulary",
  "-s        <string>        path to settings",
  "-tf       <string>        tf_frame to publish, default: '/camera'",
  "-tf-child <string>        tf_child_frame to publish, default: '/base_link'",
  "-ng       <flag>          no gui",
  "-h        <flag>          this help",
  0
};


int main(int argc, char **argv) {

  if(argc < 2 || !strcmp(argv[1], "-h")) {
    srrg_core::printBanner(banner);
  }

  // params
  std::string depth_topic_name = "/camera/depth/image_raw";
  std::string rgb_topic_name = "/camera/rgb/image_raw";
  std::string vocabulary_filename = "";
  std::string settings_filename = "";
  tf_frame = "/camera";
  tf_child_frame = "/base_link";
  bool use_gui = true;

  int c = 1;  
  while (c < argc) {
    if (!strcmp(argv[c], "-h")) {
      srrg_core::printBanner(banner);
      return 1;
    } else if (!strcmp(argv[c], "-t")) {
      c++;
      depth_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-rgbt")) {
      c++;
      rgb_topic_name = argv[c];
    } else if (!strcmp(argv[c], "-v")) {
      c++;
      vocabulary_filename = argv[c];
    } else if (!strcmp(argv[c], "-s")) {
      c++;
      settings_filename = argv[c];
    } else if (!strcmp(argv[c], "-tf")) {
      c++;
      tf_frame = argv[c];
    } else if (!strcmp(argv[c], "-tf-child")) {
      c++;
      tf_child_frame = argv[c];
    } else if (!strcmp(argv[c], "-ng")) {
      use_gui = false;
    }
    c++;
  } 
  
  std::cerr << "depth_topic    (-t):          " << depth_topic_name << std::endl;
  std::cerr << "rgb_topic      (-rgbt):       " << rgb_topic_name << std::endl;
  std::cerr << "vocabulary     (-v):          " << vocabulary_filename << std::endl;
  std::cerr << "settings       (-s):          " << settings_filename << std::endl;
  std::cerr << "tf_frame       (-tf):         " << tf_frame << std::endl;
  std::cerr << "tf_child_frame (-tf-child):   " << tf_child_frame << std::endl;
  std::cerr << "use_gui        (-ng):         " << use_gui << std::endl;

  if(vocabulary_filename.empty() || settings_filename.empty()) {
    return 1;
  }
  
  ros::init(argc, argv, "ORBSLAM2_RGBD");
  ros::start(); 
  ros::NodeHandle nh("~");
  
  ORB_SLAM2::System SLAM(vocabulary_filename, settings_filename ,ORB_SLAM2::System::RGBD,use_gui);
  ImageGrabber igb(&SLAM);
   
  message_filters::Subscriber<sensor_msgs::Image> rgb_sub(nh, rgb_topic_name, 1);
  message_filters::Subscriber<sensor_msgs::Image> depth_sub(nh, depth_topic_name, 1);
  typedef message_filters::sync_policies::ApproximateTime<sensor_msgs::Image, sensor_msgs::Image> sync_pol;
  message_filters::Synchronizer<sync_pol> sync(sync_pol(10), rgb_sub,depth_sub);
  sync.registerCallback(boost::bind(&ImageGrabber::GrabRGBD,&igb,_1,_2));

  ros::spin();

  // Stop all threads
  SLAM.Shutdown();

  // Save camera trajectory
  SLAM.SaveKeyFrameTrajectoryTUM("KeyFrameTrajectory.txt");

  ros::shutdown();

  return 0;
}

void ImageGrabber::GrabRGBD(const sensor_msgs::ImageConstPtr& msgRGB,const sensor_msgs::ImageConstPtr& msgD)
{
  // Copy the ros image message to cv::Mat.
  cv_bridge::CvImageConstPtr cv_ptrRGB;
  try
    {
      cv_ptrRGB = cv_bridge::toCvShare(msgRGB);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  cv_bridge::CvImageConstPtr cv_ptrD;
  try
    {
      cv_ptrD = cv_bridge::toCvShare(msgD);
    }
  catch (cv_bridge::Exception& e)
    {
      ROS_ERROR("cv_bridge exception: %s", e.what());
      return;
    }

  cv::Mat curr_T =  mpSLAM->TrackRGBD(cv_ptrRGB->image,cv_ptrD->image,cv_ptrRGB->header.stamp.toSec());
  if(!curr_T.rows || !curr_T.cols)
    return;
  Eigen::Matrix4f Tr, Tr_inverse;
  for(size_t i=0; i<4; ++i)
    for(size_t j=0; j<4; ++j)
      Tr(i,j) = curr_T.at<float>(i,j);

  Tr_inverse = Tr.inverse();

  Eigen::Matrix3f rot = Tr_inverse.block<3,3>(0,0);
    
  static tf::TransformBroadcaster tf_broadcaster;
  tf::Transform tf_content;
  tf::Vector3 tf_translation(Tr_inverse(0,3), Tr_inverse(1,3), Tr_inverse(2,3));
  Eigen::Quaternionf qi(rot);
  tf::Quaternion tf_quaternion(qi.x(), qi.y(), qi.z(), qi.w());
  tf_content.setOrigin(tf_translation);
  tf_content.setRotation(tf_quaternion);

  tf::StampedTransform tf_msg(tf_content, cv_ptrRGB->header.stamp, tf_frame, tf_child_frame);
  tf_broadcaster.sendTransform(tf_msg);
}


